import asyncio
import random
import numpy as np
import matplotlib.pyplot as plt

def calculate_loss_probability(buffer_capacity, processing_time, downtime_interval, recovery_time):
    total_requests = 1000  # Общее количество запросов для расчета вероятности потерь
    lost_requests = 0  # Количество потерянных запросов

    for i in range(total_requests):
        buffer = buffer_capacity
        downtime = False

        while buffer > 0:
            # Генерация запроса
            request = np.random.rand()

            if request < buffer / buffer_capacity:
                # Обработка запроса
                buffer -= 1
                processing = np.random.exponential(processing_time)
                buffer += 1  # Возвращение запроса в буфер
                buffer = min(buffer, buffer_capacity)  # Ограничение размера буфера

                if processing > processing_time:
                    # Обработка запроса заняла больше времени, чем позволяет процессор
                    downtime = True
                    break
            else:
                # Запрос потерян
                lost_requests += 1
                break

        if downtime:
            # Процессоры вышли из строя
            downtime_duration = np.random.exponential(downtime_interval)
            recovery_duration = np.random.exponential(recovery_time)
            i += downtime_duration + recovery_duration

    return lost_requests / total_requests

def simulation():
    buffer_capacities = np.arange(10, 200, 10)
    processing_times = np.arange(0.5, 5.5, 0.5)
    downtime_intervals = np.arange(1.0, 6.0, 0.5)
    recovery_times = np.arange(0.1, 1.1, 0.1)

    loss_probabilities = np.zeros((len(buffer_capacities), len(processing_times), len(downtime_intervals), len(recovery_times)))

    for i, buffer_capacity in enumerate(buffer_capacities):
        for j, processing_time in enumerate(processing_times):
            for k, downtime_interval in enumerate(downtime_intervals):
                for l, recovery_time in enumerate(recovery_times):
                    loss_probability = calculate_loss_probability(buffer_capacity, processing_time, downtime_interval, recovery_time)
                    loss_probabilities[i, j, k, l] = loss_probability

    best_indices = np.unravel_index(np.argmax(loss_probabilities), loss_probabilities.shape)
    best_buffer_capacity = buffer_capacities[best_indices[0]]
    best_processing_time = processing_times[best_indices[1]]
    best_downtime_interval = downtime_intervals[best_indices[2]]
    best_recovery_time = recovery_times[best_indices[3]]

    print("Наилучшие параметры:")
    print(f"Емкость накопителя: {best_buffer_capacity}")
    print(f"Время обработки запросов: {best_processing_time}")
    print(f"Интервал времени выхода из строя: {best_downtime_interval}")
    print(f"Время восстановления процессоров: {best_recovery_time}")

    plt.figure()
    loss = np.max(loss_probabilities, axis=3)
    loss.squeeze()
    plt.imshow(loss,cmap='hot', interpolation='nearest')
    plt.colorbar()
    plt.xticks(range(len(processing_times)), processing_times)
    plt.yticks(range(len(buffer_capacities)), buffer_capacities)
    plt.xlabel("Время обработки запросов")
    plt.ylabel("Емкость накопителя")
    plt.title("Зависимость вероятности потерь от емкости накопителя и времени обработки запросов")

    plt.show()

simulation()