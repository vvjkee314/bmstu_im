import threading
import queue
import random
import time

# Зададим параметры модели
t1 = 2
t2 = 6
t3 = 1
t4 = 2
t5 = 2
t6 = 4
t7 = 5
t8 = 1234
t9 = 89
t10 = 7
L = 10  # Емкость буфера
num_clients = 10  # Количество клиентов
num_processors = 4  # Количество процессоров
num_high_priority_clients = 2  # Количество высокоприоритетных клиентов

# Очередь для запросов
request_queue = queue.Queue(L)

qlc = 0

class Processor:
     thread=threading.Thread
     def __init__(self, status):
        self.status = status
     def crush(self):
        self.status = "crush"
     def work(self):
        self.status = "work"
     def get_status(self):
         return self.status      

# Функция для обработки запросов процессорами
def process_request(request, worker_id):
    client_id=int(request[11:])
    match client_id:
        case 1|2:
            request_interval=random.expovariate(1 / t5)
        case 3|4|5:   
            request_interval=random.expovariate(1 / t6)
        case _:
            request_interval=random.expovariate(1 / t7)
    time.sleep(request_interval)  # Время обработки запроса
    if processor_threads[worker_id].status == 'work':
        print(f"Запрос {request} обработан")

# Функция для имитации работы процессоров
def processor_worker(worker_id:int):
    while True:
        if processor_threads[worker_id].get_status()!="work":
            continue
        try:
            request = request_queue.get()  # Получение запроса из очереди
            process_request(request, worker_id)  # Обработка запроса
            request_queue.task_done()  # Отметка о завершении обработки
        except queue.Empty:
            break


# Функция для генерации запросов от клиентов
def client_worker(client_id):
    global qlc
    match client_id:
        case 1|2:
            request_interval=random.uniform(t1, t2)
        case 3|4|5:   
            request_interval=random.expovariate(1 / t3)
        case _:
            request_interval=random.expovariate(1 / t4)
                
    while True:
        time.sleep(request_interval)
        try:
            request_queue.put(f"от клиента {client_id}", block=False)
        except Exception:
            qlc += 1
            print(f'{qlc}')

# Функция для имитации отказов процессоров
def processor_failure_worker():
    while True:
        time.sleep(random.expovariate(1 / t8))  # Интервал между отказами
        failed_processors = random.sample(range(num_processors), random.randint(1, num_processors))
        for failed_processor in failed_processors:
                processor_threads[failed_processor].crush()

        print(f"Процессоры {failed_processors} вышли из строя")
        time.sleep(random.normalvariate(t9, t10))  # Время восстановления
        for failed_processor in failed_processors:
                processor_threads[failed_processor].work()
        print(f"Процессоры {failed_processors} восстановлены")
    

# Создание потоков для процессоров
processor_threads = []
for i in range(num_processors):
    processor_threads.append(Processor("work"))
    thread = threading.Thread(target=processor_worker,args=(i,))
    thread.start()
    processor_threads[i].thread=thread

# Создание потока для генерации запросов от клиентов
client_threads = []
for i in range(1,num_clients+1):
    thread = threading.Thread(target=client_worker, args=(i,))
    client_threads.append(thread)
    thread.start()

# Создание потока для имитации отказов процессоров
failure_thread = threading.Thread(target=processor_failure_worker)
failure_thread.start()
# Ожидание завершения работы всех клиентов
for thread in client_threads:
    thread.join()

# Ожидание завершения обработки всех запросов
request_queue.join()

# Остановка потоков процессоров
for thread in processor_threads:
    thread.join()

# Остановка потока отказов процессоров
failure_thread.join()