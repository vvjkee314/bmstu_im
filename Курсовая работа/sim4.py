import simpy
import random

# Параметры модели
t1 = 2
t2 = 6
t3 = 1
t4 = 2
t5 = 2
t6 = 4
t7 = 5
t8 = 1234
t9 = 89
t10 = 7
L = 10
processors = 4
failed_processors = [False, False, False, False]
request_count = 0
failed_request = 0
failure_count = 0
t11 = 0

# Функция генерации интервалов времени между запросами для каждой категории пользователей
def generate_interval(category):
    if category == 1 or category == 2:
        return random.uniform(t1, t2)
    elif category == 3 or category == 4 or category == 5:
        return random.expovariate(1 / t3)
    else:
        return random.expovariate(1 / t4)

# Функция генерации времени обслуживания запроса для каждой категории
def generate_service_time(category):
    if category == 1:
        return random.expovariate(1 / t5)
    elif category == 2:
        return random.expovariate(1 / t6)
    elif category == 3:
        return random.expovariate(1 / t7)

# Класс, представляющий сервер
class Server:
    def __init__(self, env, processors, buffer_capacity):
        self.env = env
        self.processors = processors
        self.buffer = simpy.Store(env, capacity=buffer_capacity)

    def serve_request(self, request):
        # Обслуживание запроса
        global request_count, t11, failure_time, failure_count
        category, timestamp = request
        service_time = generate_service_time(category)
        request_count += 1
        yield self.env.timeout(service_time)

    def process_failure(self):
        global failed_processors, t11, failure_time, failure_count
        while True: 
            yield self.env.timeout(random.expovariate(1/t8))
            failure_count += 1
            index = 0
            for i in range(len(failed_processors)):
                if failed_processors[i] == False:
                    index = i
                    failed_processors[i] = True
                    break
            yield self.env.timeout(random.normalvariate(t9, t10))
            failed_processors[index] = False
       
    def process_requests(self):
        global failed_processors, failed_request
        while True:
               # Получение запросов из буфера и обслуживание
            requests = []
            for i in range(self.processors):
                if failed_processors[i] == False:
                    request = yield self.buffer.get()
                    requests.append(self.env.process(self.serve_request(request)))
                else:
                    failed_request += 1


            #yield self.env.timeout(random.normalvariate(t9, t10))

            yield self.env.all_of(requests)




# Класс, представляющий клиента
class Client:
    def __init__(self, env, server, category):
        self.env = env
        self.server = server
        self.category = category

    def generate_requests(self):
        while True:
            # Генерация запроса и добавление в буфер сервера
            interval = generate_interval(self.category)
            yield self.env.timeout(interval)
            timestamp = self.env.now
            request = (self.category, timestamp)
            yield self.server.buffer.put(request)

# Класс, представляющий модель системы
class SystemModel:
    def __init__(self, L, processors):
        self.env = simpy.Environment()
        self.server = Server(self.env, processors, L)
        self.clients = []
        
        # Создание клиентов разных категорий
        for i in range(1, 11):
            if i == 1 or i == 2:
                category = 1
            elif i <= 5:
                category = 2
            else:
                category = 3
            self.clients.append(Client(self.env, self.server, category))

    def run_simulation(self, duration):
        # Запуск симуляции
        self.env.process(self.server.process_failure())
        self.env.process(self.server.process_requests())
        for client in self.clients:
            self.env.process(client.generate_requests())
        self.env.run(until=duration)

# Создание и запуск модели системы
model = SystemModel(L, processors)
model.run_simulation(4 * 60 * 60)  # 4 часа в секундах
print(f'Completed requests: {request_count}')
print(f'Failure count: {failure_count}')
print(f'Failed requsts: {failed_request}')