import asyncio
import random
import numpy as np

# Зададим параметры модели
t1 = 2
t2 = 6
t3 = 1
t4 = 2
t5 = 2
t6 = 4
t7 = 5
t8 = 1234
t9 = 89
t10 = 7
num_processors = 4
num_clients = 10
num_high_priority_clients = 2
buffer_capacity = 10
process_times = [t5, t6, t7]  # Времена обработки запросов каждой категории
failure_interval = t8
recovery_time = np.random.normal(t9, t10)

# Переменные состояния системы
buffer = asyncio.Queue(maxsize=buffer_capacity)
processors = [True] * num_processors  # True - работает, False - сломан
loss_count = 0

async def client(client):
    if client == 1 | client == 2:
        while True:
            # Запрос от пользователя 1 или 2
            interval = np.random.uniform(t1, t2)
            await asyncio.sleep(interval)
            process_time = np.random.exponential(t7)
            category = random.choice([1, 2])
            await buffer.put((category, process_time))
            
    elif client == 3 | client == 4 | client == 5:      
        while True:  
                # Запрос от пользователя 3, 4 или 5
            interval = np.random.exponential(t3)
            await asyncio.sleep(interval)
            process_time = np.random.exponential(t6)
            category = random.choice([3, 4, 5])
            await buffer.put((category, process_time))
            
    else:          # Запрос от остальных пользователей
        while True:      
            interval = np.random.exponential(t4)
            await asyncio.sleep(interval)
            process_time = np.random.exponential(t7)
            category = random.choice([6, 7, 8]) 
            await buffer.put((category, process_time))

async def processor(processor_id):
    global loss_count
    while True:
        if not processors[processor_id]:
            # Процессор сломан, ждем восстановления
            await asyncio.sleep(recovery_time)
            processors[processor_id] = True
        
        #КОГДА ОН ЛОМАЕТСЯ???!?!?!?!?!??!

        if not buffer.empty():
            category, process_time = await buffer.get()
            if processors[processor_id]:
                # Обработка запросаS
                await asyncio.sleep(process_time)
            else:
                # Пропускаем запрос из-за сломанного процессора
                loss_count += 1

async def simulation():
    # Запуск клиентов
    for i in range(num_clients):
        asyncio.create_task(client(i))
    
    # Запуск процессоров
    for i in range(num_processors):
        asyncio.create_task(processor(i))
    
    # Запуск симуляции на 4 часа
    await asyncio.sleep(4*60*60)

asyncio.run(simulation())

# Вывод результатов
print(f"Вероятность потерь запросов: {loss_count / num_clients}")

